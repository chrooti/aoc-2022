def mix(start_data, mix_key=1, mix_times=1):
    data = [(idx, num * mix_key) for idx, num in start_data]
    data_len = len(data)

    for _ in range(mix_times):
        for idx in range(data_len):
            for i in range(data_len):
                curr_idx, num = datum = data[i]
                if idx != curr_idx:
                    continue

                # this -1 was infernal, we are inserting in a list smaller by one
                # since we're going to pop this datum
                new_idx = (i + num) % (data_len - 1)
                del data[i]
                data.insert(new_idx, datum)
                break

    zero_index = None
    for i, (_, num) in enumerate(data):
        if num == 0:
            zero_index = i
            break
    assert zero_index is not None

    return sum(
        data[(zero_index + breakpoint) % data_len][1]
        for breakpoint in (1000, 2000, 3000)
    )


def main():
    with open("inputs/day20.txt", "r") as file:
        data = tuple((i, int(num)) for i, num in enumerate(file.read().splitlines()))

    return (
        mix(data),
        mix(data, 811589153, 10),
    )


if __name__ == "__main__":
    print(main())
