import importlib
import os
import sys
import time


class SolutionTestCase:
    def __init__(self, day, solution, disabled=False):
        self.day = day
        self.solution = solution
        self.disabled = disabled

    def run(self, outfile):
        day = f"day{self.day}"
        print(f"Testing {day}...")

        if self.disabled:
            print("Disabled, skipped\n")
            return 0

        try:
            module = importlib.import_module(day)
        except ModuleNotFoundError:
            print("Not found, skipped\n")
            return 0

        # patch stdout to avoid noise for ascii art tests
        stdout = sys.stdout
        sys.stdout = outfile

        # measure
        before = time.time()
        provided_solution = module.main()
        interval = time.time() - before

        # patch the real stdout back for printing
        sys.stdout = stdout

        if provided_solution == self.solution:
            print("Success")
            result = 1
        else:
            print(f"Failed, provided: {provided_solution}, actual: {self.solution}")
            result = 1

        print(f"Took {interval} seconds\n")

        return result


test_cases = [
    SolutionTestCase(1, (72017, 212520)),
    SolutionTestCase(2, (11841, 13022)),
    SolutionTestCase(3, (8243, 2631)),
    SolutionTestCase(4, (651, 956)),
    SolutionTestCase(5, ("TPGVQPFDH", "DMRDFRHHH")),
    SolutionTestCase(6, (1134, 2263)),
    SolutionTestCase(7, (1182909, 2832508)),
    SolutionTestCase(8, (1679, 536625)),
    SolutionTestCase(9, (5883, 2367)),
    SolutionTestCase(10, (13440, None)),
    SolutionTestCase(11, (72884, 15310845153)),
    SolutionTestCase(12, (425, 418)),
    SolutionTestCase(13, (5623, 20570)),
    SolutionTestCase(14, (757, 24943)),
    SolutionTestCase(15, (4827924, 12977110973564)),
    SolutionTestCase(16, (1653, 2223)),
    SolutionTestCase(17, (3219, 1582758620701)),
    SolutionTestCase(18, (4340, 2468)),
    SolutionTestCase(19, (1834, 2240), disabled=True),
    SolutionTestCase(20, (6640, 11893839037215)),
    SolutionTestCase(21, (155708040358220, 3342154812537)),
    SolutionTestCase(22, (75388, 182170), disabled=True),
    SolutionTestCase(23, (4218, 976)),
    SolutionTestCase(24, (274, 839)),
    SolutionTestCase(25, ("2-21=02=1-121-2-11-0", None)),
]


def main():
    outfile = open(os.devnull, "w")

    failed = 0
    for test_case in test_cases:
        failed += test_case.run(outfile)

    return failed
