function find_sequence_of_unique_chars(string, length)
    local counts = {}
    for i = 0, 26 do
        counts[string.char(i + string.byte("a", 1))] = 0
    end

    local current_length = 0
    for char in string:sub(1, length):gmatch(".") do
        local count = counts[char] + 1
        counts[char] = count

        if count == 1 then
            current_length = current_length + 1
        elseif count == 2 then
            current_length = current_length - 1
        end
    end

    local position = length + 1
    for char in string:sub(position):gmatch(".") do
        local count = counts[char] + 1
        counts[char] = count

        -- the count here can only get higher
        if count == 1 then
            -- if count goes from 0 to 1 we found a new unique char
            current_length = current_length + 1 -- 1
        elseif count == 2 then
            -- if count goes from 1 to 2 the char is no longer unique
            current_length = current_length - 1 -- 0
        end

        local other_boundary = position - length
        local other_boundary_char = string:sub(other_boundary, other_boundary)
        local other_boundary_count = counts[other_boundary_char] - 1
        counts[other_boundary_char] = other_boundary_count

        -- the count here can only get lower
        if other_boundary_count == 1 then
            -- if the count goes from 2 to 1 the char is unique again
            current_length = current_length + 1
        elseif other_boundary_count == 0 then
            -- if the count goes from 1 to 0 the char does not appear
            -- in the sequence anymore
            current_length = current_length - 1
        end

        if current_length == length then
            break
        end

        position = position + 1
    end

    return position
end

function main(file)
    local f = io.open(file)
    local content = f:read()
    f:close()

    print(find_sequence_of_unique_chars(content, 4))
    print(find_sequence_of_unique_chars(content, 14))
end

main("inputs/" .. debug.getinfo(1,'S').source:match("([^/@]*)%..*$") .. ".txt")
