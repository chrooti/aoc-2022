local shape_scores = {
    -- opponent, rock
    A = 1,

    -- opponent, paper
    B = 2,

    -- opponent, scissor
    C = 3,

    -- me, rock
    X = 1,

    -- me, paper
    Y = 2,

    -- me, scissor
    Z = 3,
}

local opponent_result_lut = {
    -- opponent wins
    win = {
        score = 0,
        matchups = {
            A = "Z",
            B = "X",
            C = "Y"
        }
    },

    -- draw
    draw = {
        score = 3,
        matchups = {
            A = "X",
            B = "Y",
            C = "Z"
        }
    },

    -- opponent loses
    loss = {
        score = 6,
        matchups = {
            A = "Y",
            B = "Z",
            C = "X"
        }
    }
}

function compute_final_score_if_my_play(file)
    -- interprets the second column as "what should I play?"

    local final_score = 0
    for line in io.lines(file) do
        local opponent_shape, my_shape = line:match("(%S) (%S)")
        final_score = final_score + shape_scores[my_shape]

        if opponent_result_lut.draw.matchups[opponent_shape] == my_shape then
            final_score = final_score + opponent_result_lut.draw.score
        elseif opponent_result_lut.loss.matchups[opponent_shape] == my_shape then
            final_score = final_score + opponent_result_lut.loss.score
        else
            final_score = final_score + opponent_result_lut.win.score
        end
    end

    return final_score
end

function compute_final_score_if_result(file)
    -- interprets the second column as "how should the round end for me?"

    local code_to_opponent_result = {
        X = "win",
        Y = "draw",
        Z = "loss"
    }

    local final_score = 0
    for line in io.lines(file) do
        local opponent_shape, result = line:match("(%S) (%S)")

        local starting_score = 0
        local result_info = opponent_result_lut[code_to_opponent_result[result]]
        final_score = final_score
            + result_info.score
            + shape_scores[result_info.matchups[opponent_shape]]
    end

    return final_score
end

function main(file)
    print(compute_final_score_if_my_play(file))
    print(compute_final_score_if_result(file))
end

main("inputs/" .. debug.getinfo(1,'S').source:match("([^/@]*)%..*$") .. ".txt")
