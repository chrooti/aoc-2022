local Forest = {}
Forest.mt = {
    __index = Forest,
    __newindex = function() error("Tried to set non-existing field on Forest") end
}

function Forest.new(file)
    local trees = {}

    for line in io.lines(file) do
        local line_trees = {}
        for tree in line:gmatch(".") do
            tree = tonumber(tree)
            table.insert(line_trees, tree)
        end
        table.insert(trees, line_trees)
    end

    local trees_in_line = #trees[1]

    return setmetatable({
        trees = trees,
        trees_in_line = trees_in_line,
        trees_in_internal_line = trees_in_line - 2
    }, Forest.mt)
end

function Forest:to_offset(row, col)
    return (row - 2) * self.trees_in_internal_line + (col - 2)
end

function Forest:find_visible_trees(shuffle_indices)
    local visible_trees = {}

    for i = 2, self.trees_in_internal_line + 1 do
        local row, col

        row, col = shuffle_indices(i, 1)
        local covering_from = self.trees[row][col]

        row, col = shuffle_indices(i, #self.trees)
        local covering_to = self.trees[row][col]

        local from = 2
        local to = #self.trees - 1

        while from <= to do
            if covering_from < covering_to then
                row, col = shuffle_indices(i, from)
                tree = self.trees[row][col]

                if tree > covering_from then
                    covering_from = tree
                    visible_trees[self:to_offset(row, col)] = true
                end

                from = from + 1

            else
                row, col = shuffle_indices(i, to)
                tree = self.trees[row][col]

                if tree > covering_to then
                    covering_to = tree
                    visible_trees[self:to_offset(row, col)] = true
                end

                to = to - 1

            end
        end
    end

    return visible_trees
end

function count_visible_trees(forest)
    local external_trees_count = 2 * forest.trees_in_line + 2 * forest.trees_in_internal_line

    local internal_trees_horizontal = forest:find_visible_trees(function (i, j)
        return i, j
    end)
    local internal_trees_vertical = forest:find_visible_trees(function (i, j)
        return j, i
    end)

    local internal_trees = internal_trees_horizontal
    for tree, _ in pairs(internal_trees_vertical) do
        internal_trees[tree] = true
    end

    local internal_trees_count = 0
    for _ in pairs(internal_trees) do
        internal_trees_count = internal_trees_count + 1
    end

    return external_trees_count + internal_trees_count
end

function find_most_scenic_tree(forest)

    function find_score(fixed_axis, moving_axis, end_, step, shuffle_indices)
        local row, col

        row, col = shuffle_indices(fixed_axis, moving_axis)
        local tree = forest.trees[row][col]

        for k = moving_axis + step, end_, step do
            row, col = shuffle_indices(fixed_axis, k)

            if forest.trees[row][col] >= tree then
                return math.abs(moving_axis - k)
            end
        end

        return math.abs(moving_axis - end_ + step) + 1
    end

    local dont_swap_args = function(i, j) return i, j end
    local swap_args = function(i, j) return j, i end
    local tree_count = #forest.trees

    -- assuming square forest
    local max_score = 0

    for i = 2, #forest.trees - 1 do
        for j = 2, #forest.trees - 1 do
            local tree_score = find_score(i, j, 1, -1, dont_swap_args)
                * find_score(i, j, tree_count, 1, dont_swap_args)
                * find_score(j, i, 1, -1, swap_args)
                * find_score(j, i, tree_count, 1, swap_args)

            if tree_score > max_score then
                max_score = tree_score
            end
        end
    end

    return max_score
end


function main(file)
    local forest = Forest.new(file)

    print(count_visible_trees(forest))
    print(find_most_scenic_tree(forest))
end

main("inputs/" .. debug.getinfo(1,'S').source:match("([^/@]*)%..*$") .. ".txt")
