local small_a = string.byte("a", 1)
local small_a_offset = - string.byte("a", 1) + 1
local big_a_offset = - string.byte("A", 1) + 27
local small_to_big_offset = big_a_offset - small_a_offset

function item_to_priority(item)
    local item_as_num = item:byte(1)
    return item_as_num + small_a_offset + small_to_big_offset * ((item_as_num < small_a) and 1 or 0)
end

function common_item_priority(backpack1, ...)
    local common_items = {}
    for item in backpack1:gmatch(".") do
        common_items[item] = true
    end

    for i=1, select("#", ...) do
        local new_common_items = {}
        for item in select(i, ...):gmatch(".") do
            new_common_items[item] = common_items[item]
        end
        common_items = new_common_items
    end

    for item, _ in pairs(common_items) do
        return item_to_priority(item)
    end
end

function sum_same_char_in_halves(file)
    -- find the chars that appears in both halves of the lines and sum them

    local item_sum = 0
    for line in io.lines(file) do
        item_sum = item_sum + common_item_priority(
            line:sub(1, #line / 2),
            line:sub(#line / 2 + 1, #line)
        )
    end

    return item_sum
end

function sum_same_char_in_three_lines(file)
    -- find the chars that appear in each grup of three lines and sum them

    local item_sum = 0
    local f = io.open(file, "r")
    for backpack1, backpack2, backpack3 in f:read("*all"):gmatch("([^\n]*)\n([^\n]*)\n([^\n]*)\n") do
        item_sum = item_sum + common_item_priority(
            backpack1,
            backpack2,
            backpack3
        )
    end

    return item_sum
end

function main(file)
    print(sum_same_char_in_halves(file))
    print(sum_same_char_in_three_lines(file))
end

main("inputs/" .. debug.getinfo(1,'S').source:match("([^/@]*)%..*$") .. ".txt")
