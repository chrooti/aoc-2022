function main(file)
    local entirely_overlapping_intervals = 0
    local partially_overlapping_intervals = 0
    for line in io.lines(file) do
        low1, high1, low2, high2 = line:match("(%d+)-(%d+),(%d+)-(%d+)")
        low1 = tonumber(low1)
        low2 = tonumber(low2)
        high1 = tonumber(high1)
        high2 = tonumber(high2)

        if low1 <= low2 and high1 >= high2 then
            entirely_overlapping_intervals = entirely_overlapping_intervals + 1
        elseif low1 >= low2 and high1 <= high2 then
            entirely_overlapping_intervals = entirely_overlapping_intervals + 1
        end

        if high1 >= low2 and low1 <= low2 then
            partially_overlapping_intervals = partially_overlapping_intervals + 1
        elseif high2 >= low1 and low2 <= low1 then
            partially_overlapping_intervals = partially_overlapping_intervals + 1
        end
    end

    print(entirely_overlapping_intervals)
    print(partially_overlapping_intervals)
end

main("inputs/" .. debug.getinfo(1,'S').source:match("([^/@]*)%..*$") .. ".txt")
