function stack_strategy_1_by_1(stacks, count, from, to)
    for i = 1, count do
        table.insert(stacks[to], table.remove(stacks[from]))
    end
end

function stack_strategy_batch(stacks, count, from, to)
    local stack_from = stacks[from]
    for i = count - 1, 0, -1 do
        table.insert(stacks[to], stack_from[#stack_from - i])
    end

    for i = 1, count do
        table.remove(stack_from)
    end
end

function move_stacks(file, strategy)
    local f = io.open(file)

    local stacks = {}
    for i = 1, 9 do
        stacks[i] = {}
    end

    while true do
        local line = f:read()
        if line:find("^ 1") then
            -- skip the next empty line too
            f:read()
            break
        end

        local i = 1
        for char in line:gmatch(" ?.(.).") do
            if char ~= " " then
                table.insert(stacks[i], char)
            end

            i = i + 1
        end
    end

    for _, stack in ipairs(stacks) do
        for i=1, #stack/2 do
            stack[i], stack[#stack - i + 1] = stack[#stack - i + 1], stack[i]
        end
    end

    while true do
        local line = f:read()
        if line == nil then
            break
        end

        local count, from, to = line:match("^move (%d+) from (%d) to (%d)")
        count = tonumber(count)
        from = tonumber(from)
        to = tonumber(to)

        strategy(stacks, count, from, to)
    end

    f:close()

    local output = ""
    for _, stack in pairs(stacks) do
        output = output .. stack[#stack]
    end

    return output
end

function main(file)
    print(move_stacks(file, stack_strategy_1_by_1))
    print(move_stacks(file, stack_strategy_batch))
end


main("inputs/" .. debug.getinfo(1,'S').source:match("([^/@]*)%..*$") .. ".txt")
