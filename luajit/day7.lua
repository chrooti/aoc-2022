function print_stack(stack)
    for _, entry in ipairs(stack) do
        print(entry[1], entry[2])
    end
    print("----")
end

function parse_dirs(file, on_directory_parsed)
    -- this assumes we pass through each directory only once

    local navigation_stack = {{"/", 0}}
    for line in io.lines(file) do
        local command, arg = line:match("^$ (%S+) ?(%S*)")

        if command == "cd" then
            if arg == "/" then
                -- never used apart from the first row

            elseif arg == ".." then
                -- pop

                local name, size = unpack(table.remove(navigation_stack))
                on_directory_parsed(name, size)

                local current_entry = navigation_stack[#navigation_stack]
                current_entry[2] = current_entry[2] + size

            else
                -- push
                table.insert(navigation_stack, {arg, 0})

            end

        elseif command == "ls" then
            -- ignored

        else
            local size, name = line:match("^(%S+) (%S+)")
            local current_dir_entry = navigation_stack[#navigation_stack]
            if size == "dir" then
                -- ignored

            else
                current_dir_entry[2] = current_dir_entry[2] + tonumber(size)
            end
        end
    end

    local last_size = 0
    for i = #navigation_stack, 1, -1 do
        local name, size = unpack(table.remove(navigation_stack))
        size = size + last_size

        on_directory_parsed(name, size)

        last_size = size
    end
end

function main(file)
    -- part 1
    local sum_of_sizes_under_limit = 0
    local size_limit = 100000

    -- part 2
    local dir_sizes = {}

    parse_dirs(file, function(name, size)
        -- part 1
        if size <= size_limit then
            sum_of_sizes_under_limit = sum_of_sizes_under_limit + size
        end

        -- part 2
        table.insert(dir_sizes, size)
    end)

    -- part 1
    print(sum_of_sizes_under_limit)

    -- part 2

    local root_size = dir_sizes[#dir_sizes]
    local needed_space = 30000000 - (70000000 - root_size)

    local deleted_dir_size = root_size
    for _, entry in ipairs(dir_sizes) do
        if entry > needed_space and entry < deleted_dir_size then
            deleted_dir_size = entry
        end
    end

    print(deleted_dir_size)
end

main("inputs/" .. debug.getinfo(1,'S').source:match("([^/@]*)%..*$") .. ".txt")
