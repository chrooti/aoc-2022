function main(file)
    local elves = {}
    local calories = 0
    for line in io.lines(file) do
        if #line == 0 then
            table.insert(elves, calories)
            calories = 0
        else
            calories = calories + tonumber(line)
        end
    end

    local elf_count = #elves
    table.sort(elves)

    print(elves[elf_count])
    print(elves[elf_count] + elves[elf_count - 1] + elves[elf_count - 2])
end

main("inputs/" .. debug.getinfo(1,'S').source:match("([^/@]*)%..*$") .. ".txt")
