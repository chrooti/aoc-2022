import collections
import copy


class Grid:
    def __init__(self, grid):
        self.grid = set(grid)
        self.directions = (
            (-1j, (1 + 0j, -1 + 0j)),
            (1j, (1 + 0j, -1 + 0j)),
            (-1 + 0j, (1j, -1j)),
            (1 + 0j, (1j, -1j)),
        )
        self.surroundings = tuple(
            complex(x, y)
            for y in range(-1, 2)
            for x in range(-1, 2)
            if x != 0 or y != 0
        )
        self.proposed_tiles = collections.defaultdict(list)

    @classmethod
    def from_input(cls, elves):
        grid = (
            complex(x, y)
            for y, row in enumerate(elves)
            for x, tile in enumerate(row)
            if tile == "#"
        )

        return cls(grid)

    def __contains__(self, elf):
        return elf in self.grid

    def boundaries(self):
        min_x = int(min(elf.real for elf in self.grid))
        max_x = int(max(elf.real for elf in self.grid))
        min_y = int(min(elf.imag for elf in self.grid))
        max_y = int(max(elf.imag for elf in self.grid))

        return min_x, max_x, min_y, max_y

    def round(self):
        for elf in self.grid:
            if all((elf + offset) not in self.grid for offset in self.surroundings):
                continue

            for direction, (neighbor1, neighbor2) in self.directions:
                new_elf = elf + direction

                if new_elf in self.grid:
                    continue

                if new_elf + neighbor1 in self.grid:
                    continue

                if new_elf + neighbor2 in self.grid:
                    continue

                self.proposed_tiles[new_elf].append(elf)
                break

        moved_elves = 0
        for proposed_tile, elves in self.proposed_tiles.items():
            if len(elves) > 1:
                continue

            moved_elves += 1
            self.grid.remove(elves[0])
            self.grid.add(proposed_tile)

        self.proposed_tiles.clear()
        self.directions = (*self.directions[1:], self.directions[0])

        return moved_elves

    def __repr__(self):
        min_x, max_x, min_y, max_y = self.boundaries()

        grid_container = [
            [
                "." if complex(x, y) not in self.grid else "#"
                for x in range(min_x - 1, max_x + 2)
            ]
            for y in range(min_y - 1, max_y + 2)
        ]
        return "\n".join(
            (
                "\t".join(("", *(str(i) for i in range(min_x - 1, max_x + 2)))),
                *(
                    "\t".join((str(min_y - 1 + i), *row))
                    for i, row in enumerate(grid_container)
                ),
                "=========",
            )
        )


def main():
    with open("inputs/day23.txt", "r") as file:
        elves = file.read().splitlines()

    grid = Grid.from_input(elves)

    part1_grid = copy.deepcopy(grid)
    for _ in range(10):
        part1_grid.round()

    min_x, max_x, min_y, max_y = part1_grid.boundaries()
    empty_after_ten_rounds = sum(
        int(complex(x, y) not in grid)
        for y in range(min_y, max_y + 1)
        for x in range(min_x, max_x + 1)
    )

    part2_grid = copy.deepcopy(grid)
    round_until_stopped = 1
    while part2_grid.round() != 0:
        round_until_stopped += 1

    return (empty_after_ten_rounds, round_until_stopped)


if __name__ == "__main__":
    print(main())
