import re


def main():
    with open("inputs/day4.txt") as f:
        content = f.read()

    entirely_overlapping_intervals = 0
    partially_overlapping_intervals = 0

    for l1, h1, l2, h2 in re.findall(r"(\d+)-(\d+),(\d+)-(\d+)", content):
        l1 = int(l1)
        l2 = int(l2)
        h1 = int(h1)
        h2 = int(h2)

        left = (1 << (h1 + 1)) - (1 << l1)
        right = (1 << (h2 + 1)) - (1 << l2)

        entirely_overlapping_intervals += int(
            (left & right == left) or (left & right == right)
        )
        partially_overlapping_intervals += int(left & right != 0)

    return entirely_overlapping_intervals, partially_overlapping_intervals


if __name__ == "__main__":
    print(main())
