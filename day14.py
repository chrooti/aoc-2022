import copy


def sgn(x):
    return (x > 0) - (x < 0) + (x == 0)


def count_until_grain_falls(grid, start_x, start_y, lowest_y):
    count = 0

    while True:
        x, y = start_x, start_y

        while True:
            if y + 1 > lowest_y:
                return count

            if (x, y + 1) not in grid:
                y += 1
            elif (x - 1, y + 1) not in grid:
                x -= 1
                y += 1
            elif (x + 1, y + 1) not in grid:
                x += 1
                y += 1
            else:
                grid.add((x, y))
                break

        count += 1


def count_until_grain_on_top(grid, start_x, start_y, lowest_y):
    count = 0

    while True:
        x, y = start_x, start_y

        while True:
            if y + 1 == lowest_y:
                grid.add((x, y))
                break

            if (x, y + 1) not in grid:
                y += 1
            elif (x - 1, y + 1) not in grid:
                x -= 1
                y += 1
            elif (x + 1, y + 1) not in grid:
                x += 1
                y += 1
            else:
                grid.add((x, y))
                break

        count += 1

        if start_x == x and start_y == y:
            return count


def main():
    with open("inputs/day14.txt", "r") as file:
        content = file.read().splitlines()

    start_x = 500
    start_y = 0

    rock_lines = []
    lowest_y = start_y

    for line in content:
        rock_line = []
        rock_lines.append(rock_line)
        for point in line.split(" -> "):
            x, y = point.split(",")
            x = int(x)
            y = int(y)

            rock_line.append((x, y))
            lowest_y = max(lowest_y, y)

    grid = set()
    for rock_line in rock_lines:
        last_x, last_y = rock_line[0]
        for x, y in rock_line[1:]:
            x_direction = sgn(x - last_x)
            y_direction = sgn(y - last_y)

            grid.update((i, y) for i in range(last_x, x + x_direction, x_direction))
            grid.update((x, i) for i in range(last_y, y + y_direction, y_direction))

            last_x, last_y = x, y

    return (
        count_until_grain_falls(copy.deepcopy(grid), start_x, start_y, lowest_y),
        count_until_grain_on_top(copy.deepcopy(grid), start_x, start_y, lowest_y + 2),
    )


if __name__ == "__main__":
    print(main())


# 0.028088092803955078
