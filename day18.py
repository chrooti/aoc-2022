def count_all_sides(grid, grid_size):
    offsets = ((1, 0, 0), (-1, 0, 0), (0, 1, 0), (0, -1, 0), (0, 0, 1), (0, 0, -1))

    return 6 * len(grid) - sum(
        int(all(0 <= coord < grid_size for coord in point) and point in grid)
        for x, y, z in grid
        for i, j, k in offsets
        # :))
        if (point := (x + i, y + j, z + k))
    )


def count_external_sides(grid, grid_size):
    offsets = ((1, 0, 0), (-1, 0, 0), (0, 1, 0), (0, -1, 0), (0, 0, 1), (0, 0, -1))

    visited = set()
    queue: list[tuple[int, int, int]] = [(0, 0, 0)]
    while queue:
        cube = queue.pop()
        visited.add(cube)

        x, y, z = cube
        for i, j, k in offsets:
            neighbor = (x + i, y + j, z + k)
            if neighbor in grid:
                continue

            if neighbor in visited:
                continue

            if not all(-1 <= coord < grid_size for coord in neighbor):
                continue

            queue.append(neighbor)

    return sum(
        int((x + i, y + j, z + k) in visited) for x, y, z in grid for i, j, k in offsets
    )


def main():
    with open("inputs/day18.txt", "r") as file:
        points = [square.split(",") for square in file.read().splitlines()]

    grid = set()
    grid_size = 23

    for x, y, z in points:
        x = int(x)
        y = int(y)
        z = int(z)
        grid.add((x, y, z))

    return (
        count_all_sides(grid, grid_size),
        count_external_sides(grid, grid_size),
    )


if __name__ == "__main__":
    print(main())
