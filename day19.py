import functools
import heapq
import itertools
import math
import operator
import re


class Resources:
    name_to_index = {
        "ore": 0,
        "clay": 1,
        "obsidian": 2,
        "geode": 3,
        0: 0,
        1: 1,
        2: 2,
        3: 3,
    }

    def __init__(self, ore=0, clay=0, obsidian=0, geode=0):
        self.resources = (ore, clay, obsidian, geode)

    @classmethod
    def eye(cls, value=1):
        return cls(value, value, value, value)

    def copy(self, ore=None, clay=None, obsidian=None, geode=None):
        return Resources(
            ore=ore if ore is not None else self.ore,
            clay=clay if clay is not None else self.clay,
            obsidian=obsidian if obsidian is not None else self.obsidian,
            geode=geode if geode is not None else self.geode,
        )

    def __hash__(self):
        return hash(self.resources)

    @property
    def ore(self):
        return self.resources[0]

    @property
    def clay(self):
        return self.resources[1]

    @property
    def obsidian(self):
        return self.resources[2]

    @property
    def geode(self):
        return self.resources[3]

    def iter_with_names(self):
        return zip(("ore", "clay", "obsidian", "geode"), self.resources)

    def reverse_iter_with_names(self):
        return zip(
            reversed(("ore", "clay", "obsidian", "geode")), reversed(self.resources)
        )

    def __getitem__(self, name):
        return self.resources[self.name_to_index[name]]

    def __iter__(self):
        return iter(self.resources)

    def __repr__(self):
        return repr(self.resources)

    def __add__(self, other):
        return self.copy(
            ore=self.ore + other.ore,
            clay=self.clay + other.clay,
            obsidian=self.obsidian + other.obsidian,
            geode=self.geode + other.geode,
        )

    def __sub__(self, other):
        return self.copy(
            ore=self.ore - other.ore,
            clay=self.clay - other.clay,
            obsidian=self.obsidian - other.obsidian,
            geode=self.geode - other.geode,
        )

    def __mul__(self, other):
        return self.copy(
            ore=self.ore * other.ore,
            clay=self.clay * other.clay,
            obsidian=self.obsidian * other.obsidian,
            geode=self.geode * other.geode,
        )

    def __floordiv__(self, other):
        return self.copy(
            ore=math.ceil(self.ore / other.ore) if other.ore != 0 else 0,
            clay=math.ceil(self.clay / other.clay) if other.clay != 0 else 0,
            obsidian=math.ceil(self.obsidian / other.obsidian)
            if other.obsidian != 0
            else 0,
            geode=math.ceil(self.geode / other.geode) if other.geode != 0 else 0,
        )

    @property
    def priority(self):
        return tuple(-1 * material for material in self.resources)

    def __lt__(self, other):
        return all(x < y for x, y in zip(self, other))

    def __le__(self, other):
        return all(x <= y for x, y in zip(self, other))

    def __gt__(self, other):
        return all(x > y for x, y in zip(self, other))

    def __ge__(self, other):
        return all(x >= y for x, y in zip(self, other))

    def __eq__(self, other):
        return all(x == y for x, y in zip(self, other))


ONES = {
    "ore": Resources(ore=1),
    "clay": Resources(clay=1),
    "obsidian": Resources(obsidian=1),
    "geode": Resources(geode=1),
}

ALL_OK = 0
NO_BOTS_OF_TYPE = 1
NO_TIME_BUDGET = 2


def build_material(blueprint, time_budget, bots, resources, material):
    if any(count == 0 for i, count in enumerate(bots) if blueprint[material][i] != 0):
        return NO_BOTS_OF_TYPE, None

    materials_needed = blueprint[material] - resources
    rounds_needed = 1 + max(
        max(
            needed_count
            for needed_count, blueprint_count in zip(
                materials_needed // bots, blueprint[material]
            )
            if blueprint_count != 0
        ),
        0,
    )

    if rounds_needed > time_budget:
        return NO_TIME_BUDGET, None

    new_time_budget = time_budget - rounds_needed
    new_resources = (
        resources + Resources.eye(rounds_needed) * bots - blueprint[material]
    )

    return (
        ALL_OK,
        (
            new_time_budget,
            bots + ONES[material],
            new_resources,
        ),
    )


def maximize_geodes(blueprint, time_budget):
    max_costs = {
        "ore": max(
            robot.ore for material, robot in blueprint.items() if material != "ore"
        ),
        "clay": max(
            robot.clay for material, robot in blueprint.items() if material != "clay"
        ),
        "obsidian": max(
            robot.obsidian
            for material, robot in blueprint.items()
            if material != "obsidian"
        ),
        "geode": 1 << 31,
    }

    queue = []
    visited = set()

    def enqueueable(state):
        time_budget, _, resources = state
        return (resources, time_budget, state)

    begin = (
        time_budget - 1,
        # bots
        Resources(1, 0, 0, 0),
        # resources
        Resources(1, 0, 0, 0),
    )
    heapq.heappush(queue, enqueueable(begin))
    visited.add(begin)

    max_geodes = 0
    while queue:
        *_, (time_budget, bots, resources) = heapq.heappop(queue)

        # geode
        result, new_state = build_material(
            blueprint, time_budget, bots, resources, "geode"
        )
        if result == ALL_OK:
            if new_state not in visited:
                assert new_state is not None
                visited.add(new_state)

                new_time_budget, new_bots, new_resources = new_state
                new_max_geodes = new_resources.geode + new_bots.geode * new_time_budget
                if (
                    not new_max_geodes + (new_time_budget * (new_time_budget - 1) // 2)
                    < max_geodes
                ):
                    max_geodes = max(max_geodes, new_max_geodes)
                    heapq.heappush(queue, enqueueable(new_state))

            if blueprint["geode"] <= bots:
                continue

        # other materials
        for material, bot_count in itertools.islice(bots.iter_with_names(), 3):
            if bot_count >= max_costs[material]:
                continue

            result, new_state = build_material(
                blueprint, time_budget, bots, resources, material
            )
            if result == ALL_OK and new_state not in visited:
                assert new_state is not None
                visited.add(new_state)

                new_time_budget, new_bots, new_resources = new_state
                new_max_geodes = new_resources.geode + new_bots.geode * new_time_budget

                # euler formula for turns (one geode per turn, so 1, 2, 3...)
                if (
                    new_max_geodes + (new_time_budget * (new_time_budget - 1) // 2)
                    < max_geodes
                ):
                    continue

                max_geodes = max(max_geodes, new_max_geodes)
                heapq.heappush(queue, enqueueable(new_state))

    return max_geodes


def main():
    with open("inputs/day19.txt", "r") as file:
        lines = file.read().splitlines()

    blueprints = [
        {
            out_material: Resources(
                **{
                    # can't destructure in walrus
                    # 0 -> cost, 1 -> in_material
                    cost[1]: int(cost[0])
                    for cost_str in costs_str.split(" and ")
                    if (cost := cost_str.split()) is not None
                }
            )
            for out_material, costs_str in re.findall(
                r"Each ([^ ]*) robot costs ([^\.]*)\.", line
            )
        }
        for line in lines
        if line
    ]

    return (
        sum(
            i * maximize_geodes(blueprint, 24)
            for i, blueprint in enumerate(blueprints, start=1)
        ),
        functools.reduce(
            operator.mul,
            (maximize_geodes(blueprint, 32) for blueprint in blueprints[:3]),
        ),
    )


if __name__ == "__main__":
    print(main())
