import math
import re

small_a = ord("a")
small_a_offset = -ord("a") + 1
big_a_offset = -ord("A") + 27
small_to_big_offset = big_a_offset - small_a_offset


def item_to_priority(item):
    item_as_num = ord(item)
    return (
        item_as_num + small_a_offset + small_to_big_offset * int(item_as_num < small_a)
    )


def backpack_priorities(backpack):
    priorities = 0
    for item in backpack:
        priorities |= 1 << item_to_priority(item)

    return priorities


def main():
    with open("inputs/day3.txt", "r") as file:
        content = file.read()

    # done in lua
    first_solution = 8243

    prio_sum = 0
    for backpacks in re.findall("(.*)\n(.*)\n(.*)\n", content):
        backpack_prios = [backpack_priorities(backpack) for backpack in backpacks]
        unique_item_prio = (1 << 53) - 1
        for prio in backpack_prios:
            unique_item_prio &= prio

        prio_sum += math.log2(unique_item_prio)

    return (first_solution, int(prio_sum))


if __name__ == "__main__":
    print(main())
