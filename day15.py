import re


def manhattan_distance(from_x, from_y, to_x, to_y):
    return abs(to_x - from_x) + abs(to_y - from_y)


class Interval:
    def __init__(self, low, high):
        self.low = low
        self.high = high
        self.next = None

    def extend(self, low, high):
        if self.update_if_interects(low, high):
            if not self.next:
                return

            if self.next.low > self.high:
                return

            self.high = self.next.high
            self.next = self.next.next
            return

        if self.next is not None:
            self.next.extend(low, high)
            return

        self.next = Interval(low, high)

    def covered_length(self):
        return self.high - self.low + (self.next.covered_length() if self.next else 0)

    def update_if_interects(self, low, high):
        match self.low < low, self.high > high:
            case True, True:
                return True
            case True, False:
                self.high = high
                return True
            case False, True:
                self.low = low
                return True
            case False, False:
                return False


def calculate_free_spaces_in_row(sensors_and_beacons, row):
    uint32_max = 1 << 31

    def covered_interval(sensor_and_beacon):
        sensor_x, sensor_y, beacon_x, beacon_y = sensor_and_beacon

        beacon_distance = manhattan_distance(sensor_x, sensor_y, beacon_x, beacon_y)
        row_distance = abs(sensor_y - row)
        if not row_distance < beacon_distance:
            return (uint32_max, -uint32_max)

        half_interval = beacon_distance - row_distance
        return (sensor_x - half_interval, sensor_x + half_interval)

    low, high = covered_interval(sensors_and_beacons[0])
    intervals = Interval(low, high)

    for sensor_and_beacon in sensors_and_beacons[1:]:
        low, high = covered_interval(sensor_and_beacon)
        intervals.extend(low, high)

    return intervals.covered_length()


def find_row_with_hole(sensors_and_beacons, low_bound, high_bound):
    distance_by_point = {}

    # y = x + positive_intercept (positive refers to the slope)
    positive_intercept_candidates = []

    # y = -x + negative_intercept
    negative_intercept_candidates = []

    for sensor_x, sensor_y, beacon_x, beacon_y in sensors_and_beacons:
        distance = manhattan_distance(sensor_x, sensor_y, beacon_x, beacon_y)

        distance_by_point[(sensor_x, sensor_y)] = distance

        # straight line centered on sensor and offsetted by d+1
        # (y - s_y) = (x - s_x) + d + 1 -->
        # y = (x - s_x) + d + 1 + s_y -->
        # y = x - (s_y - s_x + d + 1)
        #          ^^^^^^^^^^^^^^^^^
        positive_intercept_candidates.append(sensor_y - sensor_x + distance + 1)
        positive_intercept_candidates.append(sensor_y - sensor_x - distance - 1)

        # as above but with -(x - s_x)
        negative_intercept_candidates.append(sensor_y + sensor_x + distance + 1)
        negative_intercept_candidates.append(sensor_y + sensor_x - distance - 1)

    # nice optimization by /u/Kerma-Whore
    # we need to check only points between 4 sensors
    # so 2 positive slopes and 2 negative slopes
    positive_intercepts = {
        x
        for x in positive_intercept_candidates
        if positive_intercept_candidates.count(x) >= 2
    }
    negative_intercepts = {
        x
        for x in negative_intercept_candidates
        if negative_intercept_candidates.count(x) >= 2
    }

    # y = x + pos intersects with y = -x + neg
    # in ((pos - neg) / 2, (pos + neg) / 2)
    for neg in positive_intercepts:
        for pos in negative_intercepts:
            x = (pos - neg) // 2
            y = (pos + neg) // 2

            if not (low_bound < x < high_bound and low_bound < y < high_bound):
                continue

            is_outside_each_range = all(
                manhattan_distance(sensor_x, sensor_y, x, y)
                > distance_by_point[(sensor_x, sensor_y)]
                for sensor_x, sensor_y, _, _ in sensors_and_beacons
            )

            if is_outside_each_range:
                return high_bound * x + y


def main():
    with open("inputs/day15.txt", "r") as file:
        content = file.read()

    sensors_and_beacons = tuple(
        (int(sensor_x), int(sensor_y), int(beacon_x), int(beacon_y))
        for sensor_x, sensor_y, beacon_x, beacon_y in re.findall(
            r"Sensor at x=(\-?\d+), y=(\-?\d+): closest beacon is at x=(\-?\d+), y=(\-?\d+)",
            content,
        )
    )

    return (
        calculate_free_spaces_in_row(sensors_and_beacons, 2000000),
        find_row_with_hole(sensors_and_beacons, 0, 4000000),
    )


if __name__ == "__main__":
    print(main())
