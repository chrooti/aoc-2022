#!/bin/python3

import argparse
import glob
import os
import shutil
import subprocess
import venv


class CustomEnvBuilder(venv.EnvBuilder):
    def post_setup(self, context):
        print("* Upgrading core deps")
        self.upgrade_dependencies(context)

        print("\n* Installing requirements")
        cmd = (context.env_exec_cmd, "-Im", "pip", "install", "-r" "requirements.txt")
        subprocess.run(cmd, check=True)


this_path = os.path.dirname(os.path.realpath(__file__))
venv_dir_name = ".venv"
venv_path = f"{this_path}/{venv_dir_name}"


def setup():
    print("* Creating venv...\n")
    builder = CustomEnvBuilder(clear=True, with_pip=True, upgrade_deps=False)
    builder.create(venv_path)
    print(f'\nDone! Remember to run ". {venv_dir_name}/bin/activate"')


def clean():
    try:
        shutil.rmtree(venv_path)
    except OSError:
        pass


def lint():
    python_files = ("tasks", *glob.glob("./*.py", root_dir=this_path))

    commands = (
        ("black", *python_files),
        ("isort", *python_files, "--profile", "black"),
        ("pyflakes", *python_files),
    )
    for command in commands:
        print("*", " ".join(command))
        subprocess.run(command, stderr=subprocess.STDOUT)
        print("")


def test():
    import test as test_module

    exit(test_module.main())


def main():
    commands = {"setup": setup, "clean": clean, "lint": lint, "test": test}

    parser = argparse.ArgumentParser()
    parser.add_argument("command", choices=tuple(commands.keys()))

    args = parser.parse_args()
    commands[args.command]()


main()
