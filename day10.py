class Cpu:
    def __init__(self, instructions):
        self.register = 1
        self.cycle = 0
        self.instructions = instructions

    def execute(self, on_cycle):
        for instruction in self.instructions:
            on_cycle(self.cycle, self.register)

            match instruction:
                case ["addx", count]:
                    self.cycle += 1

                    on_cycle(self.cycle, self.register)
                    self.register += int(count)
                    self.cycle += 1
                case ["noop"]:
                    self.cycle += 1
                case _:
                    raise Exception("unknown instruction")


def count_total_signal_strength(instructions):
    total_signal_strength = 0

    def on_cycle(cycle, register):
        nonlocal total_signal_strength

        if cycle % 40 == 19:
            total_signal_strength += (cycle + 1) * register

    cpu = Cpu(instructions)
    cpu.execute(on_cycle)

    return total_signal_strength


def print_screen_output(instructions):
    screen = [" " for _ in range(240)]

    row_count = 6
    row_length = 240 // row_count

    def on_cycle(cycle, register):
        nonlocal screen

        column = cycle % row_length
        if abs(register - column) <= 1:
            screen[cycle] = "#"

    cpu = Cpu(instructions)
    cpu.execute(on_cycle)

    for i in range(row_count):
        print("".join(screen[i * row_length : (i + 1) * row_length]))


def main():
    with open("inputs/day10.txt", "r") as file:
        instructions = [line.split() for line in file.readlines()]

    return (
        count_total_signal_strength(instructions),
        print_screen_output(instructions),
    )


if __name__ == "__main__":
    print(main())
