import copy
import operator


class MonkeyTree:
    def __init__(self, solved, unsolved):
        self.solved = solved
        self.unsolved = unsolved

        # not already solved dependencies
        self.dependencies = {
            monkey_name: set(
                monkey
                for monkey in self.unsolved[monkey_name][2:]
                if monkey in self.unsolved
            )
            for monkey_name in self.unsolved
        }

        self.inverse_dependencies = {
            monkey_name: set() for monkey_name in (*self.solved, *self.unsolved)
        }
        for monkey_name in self.dependencies:
            _, _, *monkey_dependencies = self.unsolved[monkey_name]
            for monkey_dependency in monkey_dependencies:
                self.inverse_dependencies[monkey_dependency].add(monkey_name)

    @classmethod
    def from_input(cls, monkeys):
        solved = {}
        unsolved = {}

        for monkey_name, monkey in monkeys.items():
            if isinstance(monkey, int):
                solved[monkey_name] = monkey
            else:
                unsolved[monkey_name] = monkey

        return cls(solved, unsolved)

    def all_dependencies(self, first_monkey):
        dependencies = set()

        queue = [first_monkey]
        while queue:
            monkey = queue.pop()

            for dependency in self.dependencies[monkey]:
                if dependency in dependencies:
                    continue

                dependencies.add(dependency)
                queue.append(dependency)

        return dependencies

    def solve(self, root):
        assert root not in self.solved

        # find all the unsolved dependency tree for the given root
        unsolved = {
            monkey_name: monkey
            for monkey_name in self.all_dependencies(root)
            if (monkey := self.unsolved.get(monkey_name)) is not None
        }
        unsolved[root] = self.unsolved[root]

        # enqueue the first generation
        queue = list(
            set(
                inverse_dependency
                for monkey_name in self.solved
                for inverse_dependency in self.inverse_dependencies[monkey_name]
                if not self.dependencies[inverse_dependency]
                and inverse_dependency in unsolved
            )
        )

        while queue:
            monkey_name = queue.pop()
            op, _, name1, name2 = self.unsolved[monkey_name]

            del self.unsolved[monkey_name]
            self.solved[monkey_name] = op(self.solved[name1], self.solved[name2])

            for inverse_dependency in self.inverse_dependencies[monkey_name]:
                self.dependencies[inverse_dependency].remove(monkey_name)
                if not self.dependencies[inverse_dependency]:
                    queue.append(inverse_dependency)

        return self.solved[root]

    def solve_for_unknown(self, unknown):
        # solve the subtrees without humn and record the path
        upwards_path = []
        child = unknown
        while True:
            # find the parent and the sibling of the current node
            unknown_inverse_dependencies = self.inverse_dependencies[child]
            if not unknown_inverse_dependencies:
                break

            assert len(unknown_inverse_dependencies) == 1

            parent = unknown_inverse_dependencies.pop()
            is_unknown_left = self.unsolved[parent][2:].index(child) == 0
            sibling = next(
                sibling for sibling in self.unsolved[parent][2:] if sibling != child
            )
            upwards_path.append((parent, is_unknown_left, sibling))

            if sibling not in self.solved:
                # hack to avoid that the parent gets resolved
                self.dependencies[parent].add("unsolvable")

                self.solve(sibling)

            child = parent

        # walk back the path and solve for the unknown monkey

        _, _, first_sibling = upwards_path.pop()
        known_value = self.solved[first_sibling]

        for parent, is_unknown_left, sibling in reversed(upwards_path):
            op, (swap, inv_op_left, inv_op_right), _, _ = self.unsolved[parent]
            prev_known_value = known_value
            if is_unknown_left:
                prev_known_value = known_value
                a, b = swap(known_value, self.solved[sibling])
                known_value = inv_op_left(a, b)

                print(
                    f"{parent}, {prev_known_value} = {known_value} {op.__name__} {self.solved[sibling]}"
                )
            else:
                a, b = swap(known_value, self.solved[sibling])
                known_value = inv_op_right(a, b)

                print(
                    f"{parent}, {prev_known_value} = {self.solved[sibling]} {op.__name__} {known_value}"
                )

        return known_value


def main():
    ops = {
        "+": operator.add,
        "-": operator.sub,
        "*": operator.mul,
        "/": operator.floordiv,
    }

    # a = x + b --> x = a - b
    # a = x - b --> x = b + a
    # a = x * b --> x = a / b
    # a = x / b --> x = b * a

    # a = b + x --> x = a - b
    # a = b - x --> x = b - a
    # a = b * x --> x = a / b
    # a = b / x --> x = b / a

    swap = lambda x, y: (y, x)
    dont_swap = lambda x, y: (x, y)
    inv_ops = {
        # swap, if is_unknown_left, if not is_unknown_left
        "+": (dont_swap, operator.sub, operator.sub),
        "-": (swap, operator.add, operator.sub),
        "*": (dont_swap, operator.floordiv, operator.floordiv),
        "/": (swap, operator.mul, operator.floordiv),
    }

    monkeys = {}

    with open("inputs/day21.txt", "r") as file:
        for monkey in file.read().splitlines():
            match monkey.split():
                case name, name1, op, name2:
                    monkeys[name[:-1]] = (ops[op], inv_ops[op], name1, name2)
                case name, val:
                    monkeys[name[:-1]] = int(val)

    tree = MonkeyTree.from_input(monkeys)
    part1_tree = copy.deepcopy(tree)
    part2_tree = copy.deepcopy(tree)

    return (
        part1_tree.solve("root"),
        part2_tree.solve_for_unknown("humn"),
    )


if __name__ == "__main__":
    print(main())
