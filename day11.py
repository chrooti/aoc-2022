import math
import re


class WORRY_LEVEL:
    pass


class Monkey:
    def __init__(
        self, items, operator, operands, test_divider, if_true_monkey, if_false_monkey
    ):
        self.items = items
        self.operator = operator
        self.operands = operands
        self.test_divider = test_divider
        self.if_true_monkey = if_true_monkey
        self.if_false_monkey = if_false_monkey
        self.inspections = 0

    def copy(self):
        return Monkey(
            items=self.items.copy(),
            operator=self.operator,
            operands=self.operands,
            test_divider=self.test_divider,
            if_true_monkey=self.if_true_monkey,
            if_false_monkey=self.if_false_monkey,
        )

    def inspect(self, worry_level):
        self.inspections += 1
        return self.operator(
            *(
                worry_level if operand == WORRY_LEVEL else operand
                for operand in self.operands
            )
        )

    def __repr__(self):
        as_str = ", ".join(
            (
                f"items = {self.items}",
                f"operator = {self.operator}",
                f"operands = {self.operands}",
                f"test_divider = {self.test_divider}",
                f"if_true_monkey = {self.if_true_monkey}",
                f"if_false_monkey = {self.if_false_monkey}",
                f"inspections = {self.inspections}",
            )
        )

        return f"({as_str})"


class ParseException(Exception):
    pass


class Parser:
    space_eater = re.compile(r"[ \t\r\f\n]+", re.MULTILINE)
    list_separator = re.compile(r"([^,\n]*)(?:, |\n)")

    def __init__(self, content):
        self.content = content
        self.index = 0

    def expect(self, token):
        if not self.content.startswith(token, self.index):
            found = self.content[self.index : self.index + len(token)]
            raise ParseException(f'expected "{token}", found "{found}"')

        self.index += len(token)

    def eat_until(self, token):
        start_index = self.index
        token_index = self.content.find(token, self.index)

        self.index = token_index + len(token)

        return self.content[start_index : self.index - len(token)]

    def eat_until_regex(self, regex):
        match = regex.match(self.content, self.index)
        self.index += len(match[0])

        return match[1]

    def eat_spaces(self):
        match = self.space_eater.match(self.content, self.index)
        if match:
            self.index += len(match[0])

    def peek(self, offset=0, length=1):
        start_index = self.index + offset
        return self.content[start_index : start_index + length]

    def parse_operator(self, operator):
        if operator == "*":
            return lambda x, y: x * y
        elif operator == "+":
            return lambda x, y: x + y
        else:
            raise ParseException("Unknown operation")

    def parse_operand(self, operand):
        return WORRY_LEVEL if operand == "old" else int(operand)

    def parse(self):
        monkeys = []

        while self.index != len(self.content):
            self.expect("Monkey ")
            self.eat_until(":")
            self.eat_spaces()

            items = []
            self.expect("Starting items: ")
            while True:
                item = self.eat_until_regex(self.list_separator)
                items.append(int(item))
                if self.peek(-1) == "\n":
                    break
            self.eat_spaces()

            self.expect("Operation: new = ")

            first_operand = self.parse_operand(self.eat_until(" "))
            operator = self.parse_operator(self.eat_until(" "))
            second_operand = self.parse_operand(self.eat_until("\n"))
            self.eat_spaces()

            self.expect("Test: divisible by ")
            test_divider = int(self.eat_until("\n"))
            self.eat_spaces()

            self.expect("If true: throw to monkey ")
            if_true_monkey = int(self.eat_until("\n"))
            self.eat_spaces()

            self.expect("If false: throw to monkey ")
            if_false_monkey = int(self.eat_until("\n"))
            self.eat_spaces()

            monkeys.append(
                Monkey(
                    items=items,
                    operator=operator,
                    operands=(first_operand, second_operand),
                    test_divider=test_divider,
                    if_true_monkey=if_true_monkey,
                    if_false_monkey=if_false_monkey,
                )
            )

        return monkeys


def count_monkey_business(input_monkeys, rounds, after_inspection):
    monkeys = [monkey.copy() for monkey in input_monkeys]

    for _ in range(rounds):
        for monkey in monkeys:
            for item in monkey.items:
                item = after_inspection(monkey.inspect(item))
                if item % monkey.test_divider == 0:
                    monkeys[monkey.if_true_monkey].items.append(item)
                else:
                    monkeys[monkey.if_false_monkey].items.append(item)

            monkey.items.clear()

    monkeys.sort(key=lambda monkey: monkey.inspections, reverse=True)

    return monkeys[0].inspections * monkeys[1].inspections


def main():
    with open("inputs/day11.txt", "r") as file:
        content = file.read()

    parser = Parser(content)
    monkeys = parser.parse()
    test_divider_lcm = math.lcm(*(monkey.test_divider for monkey in monkeys))

    return (
        count_monkey_business(monkeys, 20, lambda level: level // 3),
        count_monkey_business(monkeys, 10000, lambda level: level % test_divider_lcm),
    )


if __name__ == "__main__":
    print(main())
