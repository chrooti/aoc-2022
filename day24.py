import collections
import heapq
import math


class Grid:
    directions = (0j, -1j, 1 + 0j, 1j, -1 + 0j)

    def __init__(
        self,
        obstacles,
        width,
        height,
        cycle,
        start_coords,
        end_coords,
    ):
        self.obstacles = obstacles
        self.width = width
        self.height = height
        self.cycle = cycle
        self.start_coords = start_coords
        self.end_coords = end_coords

    @classmethod
    def from_input(cls, content):
        content_lines = content.splitlines()

        # these refer to the internal grid!
        height = len(content_lines) - 2
        width = len(content_lines[0]) - 2
        cycle = math.lcm(height, width)

        start_coords = 1 + 1j
        end_coords = complex(width, height + 1)

        # precompute all possible blizzard positions
        obstacles = set(
            obstacle
            for y, row in enumerate(content_lines)
            for x, cell in enumerate(row)
            for obstacle in cls.parse_cell(cell, x, y, width, height, cycle)
        )

        obstacles.update((start_coords - 1j, tick) for tick in range(cycle))

        obstacles.update((end_coords + 1j, tick) for tick in range(cycle))

        return cls(
            obstacles,
            width,
            height,
            cycle,
            start_coords,
            end_coords,
        )

    @staticmethod
    def parse_cell(cell, x, y, width, height, cycle):
        match cell:
            case "<":
                return (
                    (complex((x - 1 - tick) % width + 1, y), tick)
                    for tick in range(cycle)
                )
            case ">":
                return (
                    (complex((x - 1 + tick) % width + 1, y), tick)
                    for tick in range(cycle)
                )
            case "^":
                return (
                    (complex(x, (y - 1 - tick) % height + 1), tick)
                    for tick in range(cycle)
                )
            case "v":
                return (
                    (complex(x, (y - 1 + tick) % height + 1), tick)
                    for tick in range(cycle)
                )
            case "#":
                return ((complex(x, y), tick) for tick in range(cycle))
            case _:
                return ()

    def traverse(self, start_coords, end_coords, tick):
        queue: list[tuple[int, int, tuple[complex, int]]] = []
        heapq.heappush(queue, (0, 0, (start_coords, tick)))

        lengths: dict[tuple[complex, int], int] = collections.defaultdict(
            lambda: 1 << 31
        )

        tiebreaker = 0
        while queue:
            length, _, (coords, tick) = heapq.heappop(queue)
            if coords == end_coords:
                return length

            new_tick = (tick + 1) % self.cycle
            for direction in self.directions:
                new_position = (coords + direction, new_tick)

                if new_position in self.obstacles:
                    continue

                new_length = lengths[new_position]
                if new_length <= length + 1:
                    continue

                tiebreaker += 1
                lengths[new_position] = length + 1
                heapq.heappush(queue, (length + 1, tiebreaker, new_position))

        return 1 << 31

    def traverse_forwards(self, tick=0):
        return self.traverse(self.start_coords, self.end_coords, tick)

    def traverse_backwards(self, tick=0):
        return self.traverse(self.end_coords, self.start_coords, tick)


def main():
    with open("inputs/day24.txt", "r") as file:
        grid_input = file.read()

    grid = Grid.from_input(grid_input)

    forward_traverse = grid.traverse_forwards()
    back_traverse = grid.traverse_backwards(forward_traverse)
    second_forward_traverse = grid.traverse_forwards(forward_traverse + back_traverse)

    return (
        forward_traverse,
        forward_traverse + back_traverse + second_forward_traverse,
    )


if __name__ == "__main__":
    print(main())
