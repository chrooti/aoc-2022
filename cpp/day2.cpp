// g++ -msse4 -O2 -Wall -Wextra day2.cpp

#include <immintrin.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

union pun_128 {
    __m128i_u vec;
    char chars[16];
};

void print_vec_char(__m128i_u vec) {
    pun_128 vec_union = {vec};
    printf(
        "'%c' '%c' '%c' '%c' | '%c' '%c' '%c' '%c' |"
        "'%c' '%c' '%c' '%c' | '%c' '%c' '%c' '%c' |"
        "\n",
        vec_union.chars[0], vec_union.chars[1], vec_union.chars[2], vec_union.chars[3],
        vec_union.chars[4 + 0], vec_union.chars[4 + 1], vec_union.chars[4 + 2], vec_union.chars[4 + 3],
        vec_union.chars[8 + 0], vec_union.chars[8 + 1], vec_union.chars[8 + 2], vec_union.chars[8 + 3],
        vec_union.chars[12 + 0], vec_union.chars[12 + 1], vec_union.chars[12 + 2], vec_union.chars[12 + 3]
    );
}

void print_vec_int(__m128i_u vec) {
    pun_128 vec_union = {vec};
    printf(
        "'%hhu' '%hhu' '%hhu' '%hhu' | '%hhu' '%hhu' '%hhu' '%hhu' |"
        "'%hhu' '%hhu' '%hhu' '%hhu' | '%hhu' '%hhu' '%hhu' '%hhu' |"
        "\n",
        vec_union.chars[0], vec_union.chars[1], vec_union.chars[2], vec_union.chars[3],
        vec_union.chars[4 + 0], vec_union.chars[4 + 1], vec_union.chars[4 + 2], vec_union.chars[4 + 3],
        vec_union.chars[8 + 0], vec_union.chars[8 + 1], vec_union.chars[8 + 2], vec_union.chars[8 + 3],
        vec_union.chars[12 + 0], vec_union.chars[12 + 1], vec_union.chars[12 + 2], vec_union.chars[12 + 3]
    );
}

int main() {
    FILE* f = fopen("inputs/day2.txt", "r");

    fseek(f, 0, SEEK_END);
    size_t len = ftell(f);

    char* content = reinterpret_cast<char*>(malloc(len));
    fseek(f, 0, SEEK_SET);
    fread(content, len, 1, f);

    fclose(f);

    // the score for a row is the sum of
    // - (row[0] - 65 + row[2] - 88 + 2) % 3 + 1 --> the play (plot a truth table to figure out why)
    // - (row[2] - 88) * 3 --> the result (3 draw, 6 win)

    static const __m128i_u oppo_shape_and_result_const = _mm_set_epi8(
        '\n', 'X' - 1, ' ', 'A' - 1,
        '\n', 'X' - 1, ' ', 'A' - 1,
        '\n', 'X' - 1, ' ', 'A' - 1,
        '\n', 'X' - 1, ' ', 'A' - 1
    );
    static const __m128i_u result_mask = _mm_set_epi8(
        0, 0xff, 0, 0,
        0, 0xff, 0, 0,
        0, 0xff, 0, 0,
        0, 0xff, 0, 0
    );
    static const __m128i_u result_const = _mm_set_epi8(
        0, 'X', 0, 0,
        0, 'X', 0, 0,
        0, 'X', 0, 0,
        0, 'X', 0, 0
    );
    static const __m128i_u result_const_ones = _mm_set1_epi32(1 << 16);
    static const __m128i_u threes = _mm_set1_epi16(3);

    static const __m128i_u magic = _mm_set1_epi16((1 << 9) / 3 + 1);
    static const __m128i_u threes_16 = _mm_set1_epi16(3);

    static const __m128i_u clear_upper_16_mask = _mm_set_epi32(
        0x0000ffff,
        0x0000ffff,
        0x0000ffff,
        0x0000ffff
    );

    __m128i_u oppo_shape_and_result_sum = _mm_setzero_si128();
    __m128i_u result_sum = _mm_setzero_si128();

    size_t len_rem = len % 16;
    if(len % 16 != 0) {
        __m128i_u chunk = _mm_loadu_si128(reinterpret_cast<__m128i_u*>(content));
        if(len_rem == 4) {
            chunk = _mm_and_si128(
                chunk,
                _mm_set_epi32(0, 0, 0, UINT32_MAX)
            );
        } else if(len_rem == 8) {
            chunk = _mm_and_si128(
                chunk,
                _mm_set_epi32(0, 0, UINT32_MAX, UINT32_MAX)
            );
        } else if(len_rem == 12) {
            chunk = _mm_and_si128(
                chunk,
                _mm_set_epi32(0, UINT32_MAX, UINT32_MAX, UINT32_MAX)
            );
        } else {
            __builtin_unreachable();
        }

        __m128i_u oppo_shape_and_result = _mm_sub_epi8(chunk, oppo_shape_and_result_const);
        oppo_shape_and_result = _mm_add_epi16(
            _mm_and_si128(oppo_shape_and_result, clear_upper_16_mask),
            _mm_srli_epi32(oppo_shape_and_result, 16)
        );
        __m128i_u oppo_shape_and_result_div = _mm_mullo_epi16(oppo_shape_and_result, magic);
        oppo_shape_and_result_div = _mm_srli_epi16(oppo_shape_and_result_div, 9);
        oppo_shape_and_result_div = _mm_mullo_epi16(oppo_shape_and_result_div, threes_16);
        oppo_shape_and_result = _mm_sub_epi16(
            oppo_shape_and_result,
            oppo_shape_and_result_div
        );
        oppo_shape_and_result_sum = _mm_add_epi32(oppo_shape_and_result_sum, oppo_shape_and_result);

        __m128i_u result = _mm_and_si128(chunk, result_mask);
        result = _mm_sub_epi8(result, result_const);
        result = _mm_mullo_epi16(result, threes);
        result_sum = _mm_add_epi32(result_sum, result);

        result_sum = _mm_add_epi32(result_sum, result_const_ones);
    }

    size_t i = 0;
    size_t trunc_len = len - len_rem;
    while(true) {
        __m128i_u chunk = _mm_loadu_si128(reinterpret_cast<__m128i_u*>(&content[i]));

        __m128i_u oppo_shape_and_result = _mm_sub_epi8(chunk, oppo_shape_and_result_const);
        oppo_shape_and_result = _mm_add_epi16(
            _mm_and_si128(oppo_shape_and_result, clear_upper_16_mask),
            _mm_srli_epi32(oppo_shape_and_result, 16)
        );
        __m128i_u oppo_shape_and_result_div = _mm_mullo_epi16(oppo_shape_and_result, magic);
        oppo_shape_and_result_div = _mm_srli_epi16(oppo_shape_and_result_div, 9);
        oppo_shape_and_result_div = _mm_mullo_epi16(oppo_shape_and_result_div, threes_16);
        oppo_shape_and_result = _mm_sub_epi16(
            oppo_shape_and_result,
            oppo_shape_and_result_div
        );
        oppo_shape_and_result_sum = _mm_add_epi32(oppo_shape_and_result_sum, oppo_shape_and_result);

        __m128i_u result = _mm_and_si128(chunk, result_mask);
        result = _mm_sub_epi8(result, result_const);
        result = _mm_mullo_epi16(result, threes);
        result_sum = _mm_add_epi32(result_sum, result);

        result_sum = _mm_add_epi32(result_sum, result_const_ones);

        i += 16;
        if(i >= trunc_len) {
            break;
        }
    }

    __m128i_u total = _mm_add_epi32(oppo_shape_and_result_sum, result_sum);

    total = _mm_hadd_epi16(
        total,
        total
    );
    total = _mm_hadd_epi16(
        total,
        total
    );
    total = _mm_hadd_epi16(
        total,
        total
    );

    uint64_t total_arr[2];
    _mm_storeu_pd(reinterpret_cast<double*>(total_arr), _mm_castsi128_pd(total));

    printf("%lu\n", total_arr[0] & 0xffff);

    return 0;
}
