// clang++ -mavx2 -O2 -Wall -Wextra day4.cpp
// to generate header: python day4_mask.py

#include <immintrin.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "day4_shuffle_masks.hpp"

union pun_128 {
    __m128i_u vec;
    char chars[16];
    uint16_t shorts[8];
};

void print_vec_char(__m128i_u vec) {
    pun_128 vec_union = {vec};
    printf(
        "'%c' '%c' '%c' '%c' | '%c' '%c' '%c' '%c' |"
        "'%c' '%c' '%c' '%c' | '%c' '%c' '%c' '%c' |"
        "\n",
        vec_union.chars[0], vec_union.chars[1], vec_union.chars[2], vec_union.chars[3],
        vec_union.chars[4 + 0], vec_union.chars[4 + 1], vec_union.chars[4 + 2], vec_union.chars[4 + 3],
        vec_union.chars[8 + 0], vec_union.chars[8 + 1], vec_union.chars[8 + 2], vec_union.chars[8 + 3],
        vec_union.chars[12 + 0], vec_union.chars[12 + 1], vec_union.chars[12 + 2], vec_union.chars[12 + 3]
    );
}

void print_vec_int(__m128i_u vec) {
    pun_128 vec_union = {vec};
    printf(
        "'%hhu' '%hhu' '%hhu' '%hhu' | '%hhu' '%hhu' '%hhu' '%hhu' |"
        "'%hhu' '%hhu' '%hhu' '%hhu' | '%hhu' '%hhu' '%hhu' '%hhu' |"
        "\n",
        vec_union.chars[0], vec_union.chars[1], vec_union.chars[2], vec_union.chars[3],
        vec_union.chars[4 + 0], vec_union.chars[4 + 1], vec_union.chars[4 + 2], vec_union.chars[4 + 3],
        vec_union.chars[8 + 0], vec_union.chars[8 + 1], vec_union.chars[8 + 2], vec_union.chars[8 + 3],
        vec_union.chars[12 + 0], vec_union.chars[12 + 1], vec_union.chars[12 + 2], vec_union.chars[12 + 3]
    );
}

void print_vec_int16(__m128i_u vec) {
    pun_128 vec_union = {vec};
    printf(
        "'%hu' '%hu' '%hu' '%hu' | '%hu' '%hu' '%hu' '%hu' |"
        "\n",
        vec_union.shorts[0], vec_union.shorts[1], vec_union.shorts[2], vec_union.shorts[3],
        vec_union.shorts[4 + 0], vec_union.shorts[4 + 1], vec_union.shorts[4 + 2], vec_union.shorts[4 + 3]
    );
}

// parsing based on: http://0x80.pl/articles/simd-parsing-int-sequences.html#unsinged-invalid-inputs
// thanks a bunch Wojciech

static __m128i build_digit_mask(const __m128i input) {
    // moves '0' to -128 since SSE has only signed comparison
    // TODO: maybe 0xff not needed?
    static const __m128i magic = _mm_set1_epi8(static_cast<unsigned char>(('0' + 128) & 0xff));

    // if zero is -128 then 9 is -119
    static const __m128i n118 = _mm_set1_epi8(-118);

    const __m128i moved_input = _mm_sub_epi8(input, magic);
    return _mm_cmplt_epi8(moved_input, n118);
}

int main() {
    FILE* f = fopen("inputs/day4.txt", "r");

    fseek(f, 0, SEEK_END);
    size_t len = ftell(f);

    // TODO: pad this adding at least 16 bytes
    char* content = reinterpret_cast<char*>(malloc(len));
    fseek(f, 0, SEEK_SET);
    fread(content, len, 1, f);

    fclose(f);

    static const __m128i ascii_0 = _mm_set1_epi8('0');
    static const __m128i alternate_1_10 = _mm_set_epi8(1, 10, 1, 10, 1, 10, 1, 10, 1, 10, 1, 10, 1, 10, 1, 10);
    static const __m128i newlines = _mm_set1_epi8('\n');
    static const __m128i plus_one_16 = _mm_set_epi8(
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 1, 0, 0,
        0, 1, 0, 0
    );

    size_t i = 0;
    while(true) {
        __m128i chunk = _mm_loadu_si128(reinterpret_cast<__m128i_u*>(&content[i]));

        // conversion
        __m128i chunk_normalized = _mm_subs_epi8(chunk, ascii_0);

        __m128i digit_mask = build_digit_mask(chunk);
        uint16_t digit_bitmask = _mm_movemask_epi8(digit_mask);

        const ShuffleInfo* cur_shuffle_info = &shuffle_info[digit_bitmask];

        __m128i shuffle_mask = _mm_loadu_si128(&cur_shuffle_info->mask);
        __m128i shuffled_chunk = _mm_shuffle_epi8(chunk_normalized, shuffle_mask);

        __m128i converted_chunk = _mm_maddubs_epi16(shuffled_chunk, alternate_1_10);

        __m128i chunk_tmp = _mm_add_epi8(converted_chunk, plus_one_16);
        print_vec_int(chunk_tmp);

        // TODO: unfinished

        i += cur_shuffle_info->length;
        if(i >= len) {
            break;
        }
    }

    // uint64_t total_arr[2];
    // _mm_storeu_pd(reinterpret_cast<double*>(total_arr), _mm_castsi128_pd(total));

    // printf("%lu\n", total_arr[0] & 0xffff);

    return 0;
}
