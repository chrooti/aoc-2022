import itertools

patterns = []
for pattern in itertools.product(["1", "11"], repeat=4):
    shuffle_mask = []
    i = 0
    for group in pattern:
        if group == "11":
            shuffle_mask.extend((bin(i), bin(i+1)))
            i += 3
        else:
            shuffle_mask.append(bin(i))
            i += 2

    shuffle_mask.extend((bin(255) for _ in range(16 - len(shuffle_mask))))

    shuffle_mask_str = ', '.join(reversed(shuffle_mask))
    pattern_str = '0'.join(pattern)

    for fill_pattern in itertools.product(["0", "1"], repeat=16-1-len(pattern_str)):
        fill_pattern_str = ''.join(fill_pattern)

        filled_pattern_str = f"{fill_pattern_str}0{pattern_str}"
        patterns.append(f"""
            [0b{filled_pattern_str}] = {{
                _mm_set_epi8({shuffle_mask_str}),
                {len(pattern_str) + 1}
            }},"""
        )

joined_lines = '\n'.join(patterns)
with open("day4_shuffle_masks.hpp", "w+") as f:
    f.write(f"""
struct ShuffleInfo {{
    const __m128i mask;
    uint32_t length;
}};

static const ShuffleInfo shuffle_info[65535] = {{
{joined_lines}
}};
""".lstrip())
