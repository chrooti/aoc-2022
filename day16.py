import collections
import copy
import itertools
import re


class Valve:
    def __init__(self, name, flow_rate, neighbors):
        self.name = name
        self.flow_rate = flow_rate
        self.neighbors = neighbors

    def __repr__(self):
        neighbors = ", ".join(valve for valve in self.neighbors)
        return f"{self.name}: flow_rate: {self.flow_rate}, connected: ({neighbors})"


def floyd_warshall(valves_by_name):
    distances = {(i, j): 1 << 31 for i in valves_by_name for j in valves_by_name}

    for valve in valves_by_name:
        distances[valve, valve] = 0

        for neighbor in valves_by_name[valve].neighbors:
            distances[valve, neighbor] = 1

    for valve1 in valves_by_name:
        for valve2 in valves_by_name:
            for valve3 in valves_by_name:
                tentative_distance = (
                    distances[valve1, valve2] + distances[valve2, valve3]
                )
                distances[valve1, valve3] = min(
                    distances[valve1, valve3], tentative_distance
                )

    return distances


def maximize_flow_for_paths(valves_by_name, distances, max_remaining_time):
    # compress the graph by keeping only valves with flow > 0
    compressed_valves_by_name = {
        name: valve for name, valve in valves_by_name.items() if valve.flow_rate > 0
    }
    for valve in compressed_valves_by_name.values():
        valve.neighbors = tuple(
            other_valve
            for other_valve in compressed_valves_by_name
            if other_valve != valve
        )

    # use masks instead of sets!
    valve_masks = {valve: 1 << i for i, valve in enumerate(compressed_valves_by_name)}

    # (valve, open_valves, flow, remaining_time)
    queue = collections.deque(
        (
            valve,
            valve_masks[valve],
            valves_by_name[valve].flow_rate
            * (max_remaining_time - distances["AA", valve] - 1),
            max_remaining_time - distances["AA", valve] - 1,
        )
        for valve in compressed_valves_by_name
    )

    # for each path find the max flow
    paths = {}
    while queue:
        valve, open_valves, flow, remaining_time = queue.popleft()

        paths[open_valves] = max(paths.get(open_valves, 0), flow)

        for neighbor in compressed_valves_by_name[valve].neighbors:
            open_valves_if_taken = open_valves | valve_masks[neighbor]
            if open_valves_if_taken == open_valves:
                continue

            new_remaining_time = remaining_time - distances[valve, neighbor] - 1
            if new_remaining_time <= 0:
                continue

            new_flow = (
                flow
                + new_remaining_time * compressed_valves_by_name[neighbor].flow_rate
            )

            queue.append(
                (
                    neighbor,
                    open_valves_if_taken,
                    new_flow,
                    new_remaining_time,
                )
            )

    return paths


def maximize_flow_for_me(valves_by_name, distances):
    paths = maximize_flow_for_paths(valves_by_name, distances, 30)
    return max(paths.values())


def maximize_flow_for_me_and_elephant(valves_by_name, distances):
    paths = maximize_flow_for_paths(valves_by_name, distances, 26)

    return max(
        me_flow + elephant_flow
        for (me_path, me_flow), (
            elephant_path,
            elephant_flow,
        ) in itertools.combinations(paths.items(), r=2)
        if not me_path & elephant_path
    )


def main():
    with open("inputs/day16.txt", "r") as file:
        content = file.read()

    valves_by_name = {
        name: Valve(name, int(flow_rate), tuple(neighbors.split(", ")))
        for name, flow_rate, neighbors in re.findall(
            r"Valve ([A-Z]{2}) has flow rate=(\d+); tunnels? leads? to valves? (.*)",
            content,
        )
    }

    distances = floyd_warshall(valves_by_name)

    return (
        maximize_flow_for_me(copy.deepcopy(valves_by_name), distances),
        maximize_flow_for_me_and_elephant(copy.deepcopy(valves_by_name), distances),
    )


if __name__ == "__main__":
    print(main())
