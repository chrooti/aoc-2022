D2S_LOOKUP = {
    0: ("0", 0),
    1: ("1", 0),
    2: ("2", 0),
    3: ("=", 1),
    4: ("-", 1),
}


def decimal_to_snafu(num):
    num_converted = []
    carry = 0
    while num != 0:
        num, rem = divmod(num, 5)

        rem += carry
        carry, rem = divmod(rem, 5)

        digit, carry_increment = D2S_LOOKUP[rem]
        num_converted.append(digit)
        carry += carry_increment

    if carry != 0:
        num_converted.append(str(carry))

    return "".join(reversed(num_converted))


S2D_LOOKUP = {
    "=": -2,
    "-": -1,
    "0": 0,
    "1": 1,
    "2": 2,
}


def snafu_to_decimal(num):
    num_converted = 0
    for char in num:
        num_converted *= 5
        num_converted += S2D_LOOKUP[char]

    return num_converted


def main():
    with open("inputs/day25.txt", "r") as file:
        numbers = file.read().splitlines()

    return (decimal_to_snafu(sum(snafu_to_decimal(number) for number in numbers)), None)


if __name__ == "__main__":
    print(main())
