class Forest:
    def __init__(self, path):
        with open(path, "r") as file:
            self.trees = tuple(
                tuple(int(tree) for tree in line) for line in file.read().splitlines()
            )

        self.trees_in_line = len(self.trees[0])
        self.trees_in_internal_line = self.trees_in_line - 2

    def tree_bit(self, row, column):
        return 1 << ((row - 1) * self.trees_in_internal_line + (column - 1))

    def find_visible_trees(self, shuffle_indices):
        visible_trees = 0

        for i in range(1, self.trees_in_internal_line + 1):
            row, col = shuffle_indices(i, 0)
            covering_from = self.trees[row][col]

            row, col = shuffle_indices(i, len(self.trees) - 1)
            covering_to = self.trees[row][col]

            from_ = 1
            to = len(self.trees) - 2

            while from_ <= to:
                if covering_from < covering_to:
                    row, col = shuffle_indices(i, from_)
                    tree = self.trees[row][col]

                    if tree > covering_from:
                        covering_from = tree
                        visible_trees |= self.tree_bit(row, col)

                    from_ += 1

                else:
                    row, col = shuffle_indices(i, to)
                    tree = self.trees[row][col]

                    if tree > covering_to:
                        covering_to = tree
                        visible_trees |= self.tree_bit(row, col)

                    to -= 1

        return visible_trees


def count_visible_trees(forest):
    external_trees = 2 * forest.trees_in_line + 2 * forest.trees_in_internal_line
    internal_trees = (
        forest.find_visible_trees(lambda i, j: (i, j))
        | forest.find_visible_trees(lambda i, j: (j, i))
    ).bit_count()

    return external_trees + internal_trees


def find_most_scenic_tree(forest):
    scenic_score = 0

    def find_score(fixed_axis, moving_axis, end, step, shuffle_indices):
        row, col = shuffle_indices(fixed_axis, moving_axis)
        tree = forest.trees[row][col]

        for k in range(moving_axis + step, end, step):
            row, col = shuffle_indices(fixed_axis, k)

            if forest.trees[row][col] >= tree:
                return abs(moving_axis - k)

        return abs(moving_axis - end + step)

    dont_swap_args = lambda i, j: (i, j)
    swap_args = lambda i, j: (j, i)
    tree_count = len(forest.trees)
    for i in range(1, len(forest.trees) - 1):
        for j in range(1, len(forest.trees) - 1):
            tree_score = (
                find_score(i, j, -1, -1, dont_swap_args)
                * find_score(i, j, tree_count, 1, dont_swap_args)
                * find_score(j, i, -1, -1, swap_args)
                * find_score(j, i, tree_count, 1, swap_args)
            )

            if scenic_score < tree_score:
                scenic_score = tree_score

    return scenic_score


def main():
    forest = Forest("inputs/day8.txt")

    return (count_visible_trees(forest), find_most_scenic_tree(forest))


if __name__ == "__main__":
    print(main())
