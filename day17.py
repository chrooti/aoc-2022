class Piece:
    def __init__(self, parts):
        self.parts = parts
        self.height = max(y for _, y in parts)

    def __getitem__(self, index):
        return self.parts[index]


def simulate(pieces, moves, rounds):
    height = 0
    cache = {}
    filled = set()

    def can_move(piece, x, y, offset_x, offset_y):
        for part_x, part_y in piece:
            new_x = x + offset_x + part_x
            new_y = y + offset_y + part_y

            if not 0 <= new_x < 7:
                return False

            if new_y <= 0:
                return False

            if (new_x, new_y) in filled:
                return False

        return True

    piece_index = 0
    move_index = 0
    for round_ in range(rounds):
        # run of the mill simulation

        x = 2
        y = height + 4

        piece = pieces[piece_index]
        piece_index = (piece_index + 1) % len(pieces)
        while True:
            move = moves[move_index]
            move_index = (move_index + 1) % len(moves)

            if can_move(piece, x, y, move, 0):
                x += move

            if not can_move(piece, x, y, 0, -1):
                break

            y -= 1

        filled.update((x + part_x, y + part_y) for part_x, part_y in piece)
        height = max(height, y + piece.height)

        # I wish I were that smart, this idea is from u/4HbQ
        # I just simulated 5k lines and used tortoise and hare
        key = piece_index, move_index
        match cache.get(key):
            # if:
            # - we have found the piece/move pair again
            # - can get _exactly_ to the top repeating the cycle N times
            case first_round, first_height:
                remaining_rounds = rounds - round_
                cycle_rounds = round_ - first_round
                cycle_count, overshoot = divmod(remaining_rounds, cycle_rounds)
                if overshoot == 0:
                    cycle_height = height - first_height
                    return height + cycle_height * cycle_count - 1
            case _:
                # record the piece/move where we find each piece
                cache[key] = round_, height

    return height


def main():
    # I swear I will always use complexes for 2D grids from now on

    with open("inputs/day17.txt", "r") as file:
        # so lucky that "=" is between "<" and ">"
        moves = [ord(move) - ord("=") for move in file.read()]

    # x, y; 0 is bottom row
    pieces = (
        Piece(((0, 0), (1, 0), (2, 0), (3, 0))),
        Piece(((1, 0), (0, 1), (2, 1), (1, 2))),
        Piece(((0, 0), (1, 0), (2, 0), (2, 1), (2, 2))),
        Piece(((0, 0), (0, 1), (0, 2), (0, 3))),
        Piece(((0, 0), (0, 1), (1, 0), (1, 1))),
    )

    return (
        simulate(pieces, moves, 2022),
        simulate(pieces, moves, 1000000000000),
    )


if __name__ == "__main__":
    print(main())
