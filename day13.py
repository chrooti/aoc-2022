import copy


class Cell:
    def __init__(self, cells):
        self.cells = cells

    def __lt__(self, other):
        return compare_cells_lt(self.cells, other.cells)

    def __repr__(self):
        return repr(self.cells)

    @classmethod
    def from_line(cls, line):
        i = 0

        value_stack = []
        while True:
            if line[i] == "[":
                value_stack.append([])
                i += 1
            elif line[i] == "]":
                last_value = value_stack.pop()

                if not value_stack:
                    return cls(last_value)

                value_stack[-1].append(last_value)
                i += 1

                if line[i] == ",":
                    i += 1
            else:
                number = 0
                while line[i].isdigit():
                    number *= 10
                    number += int(line[i])
                    i += 1

                value_stack[-1].append(number)

                if line[i] == ",":
                    i += 1


def compare_cells_lt(cells1, cells2):
    for cell1, cell2 in zip(cells1, cells2):
        if isinstance(cell1, list):
            result = compare_cells_lt(
                cell1, cell2 if isinstance(cell2, list) else [cell2]
            )

            if result is not None:
                return result

        elif isinstance(cell2, list):
            result = compare_cells_lt(
                cell1 if isinstance(cell1, list) else [cell1], cell2
            )
            if result is not None:
                return result

        elif cell1 < cell2:
            return True

        elif cell1 > cell2:
            return False

    if len(cells1) < len(cells2):
        return True
    elif len(cells1) > len(cells2):
        return False


def pairwise(iterable):
    iterator = iter(iterable)
    return zip(iterator, iterator)


def count_ordered_cell_pairs(cells):
    return sum(
        i * int(cell1 < cell2)
        for i, (cell1, cell2) in enumerate(pairwise(cells), start=1)
    )


def find_decoder_key(cells):
    divider1 = Cell([[2]])
    divider2 = Cell([[6]])

    cells.extend((divider1, divider2))
    cells.sort()

    return (cells.index(divider1) + 1) * (cells.index(divider2) + 1)


def main():
    with open("inputs/day13.txt", "r") as file:
        lines = file.read().splitlines()

    cells = [Cell.from_line(line) for line in lines if line]

    return (
        count_ordered_cell_pairs(copy.deepcopy(cells)),
        find_decoder_key(copy.deepcopy(cells)),
    )


if __name__ == "__main__":
    print(main())
