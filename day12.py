import collections


class Node:
    def __init__(self, x, y, height, distance):
        self.x = x
        self.y = y
        self.height = height
        self.distance = distance
        self.visited = False
        self.processed = False

    def __lt__(self, other):
        return self.distance < other.distance

    def __repr__(self):
        as_str = ", ".join(
            (
                f"x = {self.x}",
                f"y = {self.y}",
                f"height = {self.height}",
                f"distance = {self.distance}",
                f"visited = {self.visited}",
            )
        )
        return f"({as_str})"


class Grid:
    MIN_HEIGHT = ord("a")
    MAX_HEIGHT = ord("z")
    START_HEIGHT = ord("S")
    END_HEIGHT = ord("E")

    MAX_DISTANCE = 1 << 31

    def __init__(self, nodes, start_heights):
        self.nodes = []
        self.start_nodes = []

        for y, line in enumerate(nodes):
            grid_line = []
            self.nodes.append(grid_line)

            for x, height in enumerate(line):
                if height in start_heights:
                    node = Node(
                        x=x,
                        y=y,
                        height=self.MIN_HEIGHT,
                        distance=0,
                    )
                    self.start_nodes.append(node)
                    grid_line.append(node)
                elif height == self.END_HEIGHT:
                    node = Node(
                        x=x,
                        y=y,
                        height=self.MAX_HEIGHT,
                        distance=self.MAX_DISTANCE,
                    )
                    self.end_node = node
                    grid_line.append(node)
                else:
                    grid_line.append(
                        Node(
                            x=x,
                            y=y,
                            height=height,
                            distance=self.MAX_DISTANCE,
                        )
                    )

        assert self.start_nodes
        assert self.end_node is not None


def djikstra(nodes, start_node, end_node):
    q = collections.deque((start_node,))

    while q:
        current_node = q.popleft()
        if current_node == end_node:
            break

        for dx, dy in ((0, 1), (0, -1), (1, 0), (-1, 0)):
            x1 = current_node.x + dx
            y1 = current_node.y + dy
            if x1 < 0 or y1 < 0 or x1 >= len(nodes[0]) or y1 >= len(nodes):
                continue

            neighbor_node = nodes[y1][x1]
            if neighbor_node.visited:
                continue

            if neighbor_node.height - current_node.height > 1:
                continue

            if not neighbor_node.processed:
                neighbor_node.processed = True
                q.append(neighbor_node)

            if (
                tentative_distance := current_node.distance + 1
            ) < neighbor_node.distance:
                neighbor_node.distance = tentative_distance

        current_node.visited = True

    return end_node.distance


def find_min_distance(nodes, start_heights):
    grid = Grid(nodes=nodes, start_heights=start_heights)

    return min(djikstra(grid.nodes, node, grid.end_node) for node in grid.start_nodes)


def main():
    with open("inputs/day12.txt", "r") as file:
        nodes = tuple(
            tuple(ord(char) for char in line) for line in file.read().splitlines()
        )

    return (
        find_min_distance(nodes, (ord("S"),)),
        find_min_distance(nodes, (ord("S"), ord("a"))),
    )


if __name__ == "__main__":
    print(main())
