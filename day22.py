import collections
import re

# def main():
#     with open("inputs/day22.txt", "r") as file:
#     # with open("test.txt", "r") as file:
#         grid_input, moves_input = file.read().split("\n\n")

#     grid_rows = grid_input.splitlines()
#     moves = [
#         match
#         for move in re.findall(r"(\d+)|(L)|(R)", moves_input)
#         for match in move
#         if match != ''
#     ]

#     # x, y, is_wall
#     grid = {
#         complex(x, y): char == "#"
#         for y, row in enumerate(grid_rows, start=1)
#         for x, char in enumerate(row, start=1)
#         if char != " "
#     }
#     grid_width = int(max(point.real for point in grid))
#     grid_height = int(max(point.imag for point in grid))

#     grid_boundaries_row = {
#         y: (
#             # min
#             next(
#                 complex(x, y)
#                 for x in range(1, grid_width+1)
#                 if complex(x, y) in grid
#             ),
#             next(
#                 complex(x, y)
#                 for x in reversed(range(1, grid_width+1))
#                 if complex(x, y) in grid
#             ),
#         )
#         for y in range(1, grid_height+1)
#     }

#     grid_boundaries_col = {
#         x: (
#             # min
#             next(
#                 complex(x, y)
#                 for y in range(1, grid_height+1)
#                 if complex(x, y) in grid
#             ),
#             next(
#                 complex(x, y)
#                 for y in reversed(range(1, grid_height+1))
#                 if complex(x, y) in grid
#             ),
#         )
#         for x in range(1, grid_width+1)
#     }

#     def wrap_to_boundaries(point, direction):
#         match direction:
#             case 1:
#                 return grid_boundaries_row[int(point.imag)][0]
#             case 1j:
#                 return grid_boundaries_col[int(point.real)][0]
#             case -1:
#                 return grid_boundaries_row[int(point.imag)][1]
#             case -1j:
#                 return grid_boundaries_col[int(point.real)][1]
#             case _:
#                 raise Exception()


#     # x, y (right down left top)
#     directions = [1+0j, 1j, -1+0j, -1j]
# position: complex = complex(
#     min(point.real for point in grid if point.imag == 1),
#     1
# )

# direction = 0

# # cut "\n"
# for move in moves:
#     match move:
#         case "L":
#             direction = (direction - 1) % len(directions)
#         case "R":
#             direction = (direction + 1) % len(directions)
#         case count_str:
#             count = int(count_str)
#             for _ in range(count):
#                 new_position = position + directions[direction]
#                 if new_position not in grid:
#                     new_position = wrap_to_boundaries(new_position, directions[direction])

#                 # break on wall
#                 if grid[new_position]:
#                     break

#                 position = new_position

# return (
#     position.imag * 1000 + position.real * 4 + direction
# )


class Face:
    def __init__(self, points):
        self.points = points

        self.min_x = int(min(point.real for point in points))
        self.max_x = int(max(point.real for point in points))
        self.min_y = int(min(point.imag for point in points))
        self.max_y = int(max(point.imag for point in points))

        self.converted_points = {
            complex(point.real - self.min_x, point.imag - self.min_y): is_wall
            for point, is_wall in points.items()
        }

        # right, down, left, top
        # each one is (face, count of clockwise rotations)
        self.neighbors = [None, None, None, None]

        # top left, top right, bot right, bot left
        self.corners = [None, None, None, None]

        self.row_boundaries = {
            y: (
                # min
                next(
                    complex(x, y)
                    for x in range(self.min_x, self.max_x + 1)
                    if complex(x, y) in points
                ),
                next(
                    complex(x, y)
                    for x in reversed(range(self.min_x, self.max_x + 1))
                    if complex(x, y) in points
                ),
            )
            for y in range(self.min_y, self.max_y + 1)
        }

        self.col_boundaries = {
            x: (
                # min
                next(
                    complex(x, y)
                    for y in range(self.min_y, self.max_y + 1)
                    if complex(x, y) in points
                ),
                next(
                    complex(x, y)
                    for y in reversed(range(self.min_y, self.max_y + 1))
                    if complex(x, y) in points
                ),
            )
            for x in range(self.min_x, self.max_x + 1)
        }

    def __contains__(self, point):
        return point in self.converted_points

    def __repr__(self):
        return repr(self.points)

    def wrap_to_converted_boundaries(
        self, point, direction_from, direction_to, direction_of_entry
    ):
        max_x_converted = self.max_x - self.min_x
        max_y_converted = self.max_y - self.min_y

        print(direction_of_entry)
        if direction_of_entry in (2, 3):
            if abs(direction_to - direction_from) in (1, 2):
                point = complex(max_x_converted - point.real, point.imag)

            if abs(direction_to - direction_from) in (2, 3):
                point = complex(point.real, max_y_converted - point.imag)
        else:
            if abs(direction_to - direction_from) in (3, 2):
                point = complex(max_x_converted - point.real, point.imag)

            if abs(direction_to - direction_from) in (2, 1):
                point = complex(point.real, max_y_converted - point.imag)

        if (direction_from - direction_to) % 2 != 0:
            # swap x, y
            point = complex(point.imag, point.real)

        # right, down, left, top

        # direction of entry è l'indice
        # della faccia corrente nei neighbors
        match direction_of_entry:
            case 0:
                return complex(self.max_x - self.min_x, point.imag)
            case 1:
                return complex(point.real, self.max_y - self.min_y)
            case 2:
                return complex(0, point.imag)
            case 3:
                return complex(point.real, 0)
            case _:
                raise Exception()

    def wrap_to_boundaries(self, point, direction):
        match direction:
            case 1:
                return self.row_boundaries[int(point.imag)][0]
            case 1j:
                return self.col_boundaries[int(point.real)][0]
            case -1:
                return self.row_boundaries[int(point.imag)][1]
            case -1j:
                return self.col_boundaries[int(point.real)][1]
            case _:
                raise Exception()


def main():
    # with open("inputs/day22.txt", "r") as file:
    with open("test.txt", "r") as file:
        grid_input, moves_input = file.read().split("\n\n")

    grid_rows = grid_input.splitlines()
    moves = [
        match
        for move in re.findall(r"(\d+)|(L)|(R)", moves_input)
        for match in move
        if match != ""
    ]

    faces_per_row = 3
    face_rows = 4
    face_size = max(len(row) for row in grid_rows) // faces_per_row
    faces = []
    face_map = [[-1 for _ in range(faces_per_row)] for _ in range(face_rows)]

    for y in range(face_rows):
        for x in range(faces_per_row):
            top_left_x = x * face_size
            top_left_y = y * face_size

            if top_left_x >= len(grid_rows[top_left_y]):
                continue

            if grid_rows[top_left_y][top_left_x] == " ":
                continue

            face_map[y][x] = len(faces)

            bottom_left_y = top_left_y + face_size
            top_right_x = top_left_x + face_size

            faces.append(
                Face(
                    {
                        # TODO +1
                        complex(x, y): grid_rows[y][x] == "#"
                        for y in range(top_left_y, bottom_left_y)
                        for x in range(top_left_x, top_right_x)
                    }
                )
            )

    assert len(faces) == 6

    print(
        *(" ".join(str(x) if x != -1 else "." for x in row) for row in face_map),
        sep="\n"
    )

    # assign vertices to each face

    # x, y (right down left top)
    directions = [1 + 0j, 1j, -1 + 0j, -1j]
    dimensions = 3
    faces[0].corners = [
        (0, 0, 0),  # top, left2
        (0, 1, 0),  # right, top2
        (1, 1, 0),  # down, right2
        (1, 0, 0),  # left, down2
    ]
    queue: list[tuple[int, complex, int]] = [
        (
            0,
            next(
                complex(x, y)
                for y in range(face_rows)
                for x in range(faces_per_row)
                if face_map[y][x] == 0
            ),
            next(
                i
                for i in range(dimensions)
                if len(set(corner[i] for corner in faces[0].corners)) == 1
            ),
        )
    ]
    visited = {0}
    while queue:
        face_id, position, fixed_axis = queue.pop()

        face = faces[face_id]
        corners = face.corners

        for i, off in enumerate(directions):
            x = int(position.real + off.real)
            y = int(position.imag + off.imag)

            if not 0 <= x < faces_per_row:
                continue

            if not 0 <= y < face_rows:
                continue

            new_face = face_map[y][x]
            if new_face == -1:
                continue

            if new_face in visited:
                continue

            visited.add(face_id)

            same_side1 = (i + 1) % len(directions)
            same_side2 = (i + 2) % len(directions)
            opposite_side1 = i
            opposite_side2 = (i - 1) % len(directions)

            new_face_corners = faces[new_face].corners

            new_face_corners[opposite_side1] = corners[same_side1]
            new_face_corners[opposite_side2] = corners[same_side2]

            new_fixed_axis = next(
                j
                for j in range(dimensions)
                if len(
                    set(corner[j] for corner in new_face_corners if corner is not None)
                )
                == 1
                and j != fixed_axis
            )

            new_face_corners[same_side1] = tuple(
                x if i == new_fixed_axis else 1 - x
                for i, x in enumerate(new_face_corners[opposite_side2])
            )
            new_face_corners[same_side2] = tuple(
                x if i == new_fixed_axis else 1 - x
                for i, x in enumerate(new_face_corners[opposite_side1])
            )

            queue.append((new_face, complex(x, y), new_fixed_axis))

    # transform vertices into sides
    vertices = collections.defaultdict(list)
    for i, face in enumerate(faces):
        for j, corner in enumerate(face.corners):
            next_corner = face.corners[(j + 1) % len(directions)]
            sorted_corners = tuple(sorted((corner, next_corner)))
            vertices[sorted_corners].append((i, (j - 1) % len(directions)))

    # match neighbors
    for corners, ((face1, direction1), (face2, direction2)) in vertices.items():
        faces[face1].neighbors[direction1] = (
            face2,
            (direction1 + 2 - direction2) % len(directions),
        )
        faces[face2].neighbors[direction2] = (
            face1,
            (direction2 + 2 - direction1) % len(directions),
        )

    # do the walking
    position: complex = 0j

    for i, face in enumerate(faces):
        print(i, face.neighbors)

    # for face in faces:
    #     print(*(str(x) for x in face.converted_points.keys()))

    direction = 0
    face_id = 0
    face = faces[face_id]
    print(face_id, complex(position.real + face.min_x, position.imag + face.min_y))
    for move in moves:
        match move:
            case "L":
                direction = (direction - 1) % len(directions)
            case "R":
                direction = (direction + 1) % len(directions)
            case count_str:
                count = int(count_str)
                print(count, direction)
                for _ in range(count):
                    new_position = position + directions[direction]

                    new_face_id = face_id
                    new_face = faces[new_face_id]
                    new_direction = direction
                    if new_position not in face:
                        new_face_id, rotations = face.neighbors[direction]
                        new_face = faces[new_face_id]
                        if rotations == 3:
                            rotations = 1
                        elif rotations == 1:
                            rotations = 3
                        new_direction = (direction + rotations) % len(directions)

                        new_position = new_face.wrap_to_converted_boundaries(
                            new_position,
                            direction,
                            new_direction,
                            next(
                                i
                                for i, (x, _) in enumerate(new_face.neighbors)
                                if x == face_id
                            )
                            # new_face.neighbors.index(face_id)
                        )

                    # break on wall
                    if new_face.converted_points[new_position]:
                        break

                    position = new_position
                    face_id = new_face_id
                    face = new_face
                    direction = new_direction
                    print(face_id, complex(position.real, position.imag))
                print("--")

    position = complex(position.real + face.min_x + 1, position.imag + face.min_y + 1)

    return position.imag * 1000 + position.real * 4 + direction


if __name__ == "__main__":
    print(main())
