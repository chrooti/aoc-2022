def find_sequence_of_unique_chars(string, length):
    bitmask = 0
    for char in string[:length]:
        bitmask ^= 1 << (ord(char) - ord("a"))

    if bitmask.bit_count() == length:
        return length

    for i, char in enumerate(string[length:], start=length):
        bitmask ^= 1 << (ord(char) - ord("a"))
        bitmask ^= 1 << (ord(string[i - length]) - ord("a"))
        if bitmask.bit_count() == length:
            return i + 1


def main():
    with open("inputs/day6.txt", "r") as file:
        content = file.read()

    return (
        find_sequence_of_unique_chars(content, 4),
        find_sequence_of_unique_chars(content, 14),
    )


if __name__ == "__main__":
    print(main())
