DIRECTIONS = {"U": (0, -1), "D": (0, 1), "L": (-1, 0), "R": (1, 0)}


def parse_move(line):
    direction, amount = line.split()
    return DIRECTIONS[direction], int(amount)


def sgn_or_zero(x):
    return int(x > 0) - int(x < 0)


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return f"({self.x}, {self.y})"

    def as_tuple(self):
        return (self.x, self.y)

    def move(self, x, y):
        self.x += x
        self.y += y

    def follow(self, other):
        x = other.x - self.x
        y = other.y - self.y

        if abs(x) <= 1 and abs(y) <= 1:
            return

        self.x += sgn_or_zero(x)
        self.y += sgn_or_zero(y)


def count_tail_visited_positions(moves, tail_length):
    visited_positions = set()
    head = Point(0, 0)
    knots = tuple(Point(0, 0) for _ in range(tail_length))

    for (x, y), amount in moves:
        for _ in range(amount):
            head.move(x, y)
            last_knot = head
            for knot in knots:
                knot.follow(last_knot)
                last_knot = knot

            visited_positions.add(last_knot.as_tuple())

    return len(visited_positions)


def main():
    with open("inputs/day9.txt", "r") as file:
        moves = tuple(parse_move(line) for line in file.readlines())

    return (
        count_tail_visited_positions(moves, 1),
        count_tail_visited_positions(moves, 9),
    )


if __name__ == "__main__":
    print(main())
