## Advent of code 2022

Project structure:
- `/` python solutions (from day 3)
- `cpp/` cpp solutions + helpers, SIMD only (day 2, day 4 incomplete)
- `inputs/` input files
- `luajit/` luajit solutions (from day 1 to day 8)

Run `./tasks` if you need a helper to setup/clean/lint the project.
